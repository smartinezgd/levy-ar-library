# Levy AR Library #

Levy AR Library (named from a [netrunner card](https://netrunnerdb.com/en/card/03035) is a graphical tool for organizing scientific papers. It is meant to be simple and with much less options than more advanced paper mangement software.

## License ##

Levy AR Library source code is published under GPLv3 license (see LICENSE.txt)

The icons used by the graphical interface were made by Lauren Reen and are distrubuted following the [*attribution* Creative Commons License](https://creativecommons.org/licenses/by/3.0/) (see CREDITS.txt)

## Dependencies ##

Levy AR Library uses Python3, SQLite3 and PyQt5. It requires the
python-pyqt5 library and possibly a pysqlite3 library if your Python
distribution does not embeds it.

### Installation ###

Simply run

`python setup.py install`. You may use the --prefix parameter to install in a custom directory

