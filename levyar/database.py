import sqlite3
import os

#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


class View(object):
    def __init__(self,db_result):
        self.papers = []
        for line in db_result:
            self.papers.append({"title":line[0],"id":line[1]})

        
class Database(object):
    schema = {"authors":{"id":"INTEGER","name":"TEXT"},
              "tags":{"id":"INTEGER","name":"TEXT"},
              "papers":{"id":"INTEGER","title":"TEXT","path":"TEXT","notes":"TEXT","bibtex":"TEXT"},
              "publications":{"id":"INTEGER","name":"TEXT"},
              "ap":{"aid":"INTEGER","pid":"INTEGER"},
              "tp":{"tid":"INTEGER","pid":"INTEGER"},
              "pubp":{"pubid":"INTEGER","pid":"INTEGER"}}

    def __init__(self,db_path):
        self.db_path = db_path
        self.connection = None
        
    def connect(self):
        conn = sqlite3.connect(self.db_path)
        self.connection = conn
        cursor = conn.cursor()
        return cursor

    def commit_and_disconnect(self):
        self.connection.commit()
        self.connection.close()
        self.connection = None

    def discard_and_disconnect(self):
        self.connection.close()
        self.connection = None

    def check_database(self):
        """if the file given for database does not exist, creates it,
        else, returns True if the supplied file is a database with
        correct schema, False if not"""
        if not os.path.exists(self.db_path):
            self.create_database()
            return True
        else:
            try:
                cn = sqlite3.connect('file:'+self.db_path+"?mode=ro", uri=True)
                cn.close()
            except sqlite3.OperationalError:
                return False
            c = self.connect()
            tables = [x[0] for x in c.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall()]
            for t in Database.schema.keys():
                if t not in tables:
                    return False
                schema = c.execute("PRAGMA table_info('"+t+"')").fetchall()
                if not self.check_table(t,schema):
                    self.discard_and_disconnect()
                    return False
            self.discard_and_disconnect()
            return True

    def check_table(self,table,schema):
        refschema = Database.schema[table]
        curschema = {}
        for column in schema:
            curschema[column[1]] = column[2]
        return refschema == curschema


    def create_database(self):
        if os.path.exists(self.db_path):
            os.path.remove(self.db_path)
        if not os.path.exists(os.path.dirname(self.db_path)):
            os.makedirs(os.path.dirname(self.db_path))
        c = self.connect()
        for table in Database.schema.keys():
            req = "CREATE TABLE "+table+"("
            inp = []
            for field in Database.schema[table].keys():
                if field == "id":
                    inp.append("id INTEGER PRIMARY KEY AUTOINCREMENT")
                else:
                    inp.append(field+" "+Database.schema[table][field])
            req+=",".join(inp)
            req+=")"
            c.execute(req)
        self.commit_and_disconnect()
        
    
    
    def get_all_tags(self):
        c = self.connect()
        r = c.execute("SELECT name FROM tags")
        res = [x[0] for x in r.fetchall()]
        self.discard_and_disconnect()
        return res

    def get_all_tags_with_id(self):
        c = self.connect()
        r = c.execute("SELECT id, name FROM tags")
        res = [{"id":x[0],"name":x[1]} for x in r.fetchall()]
        self.discard_and_disconnect()
        return res
        
    def get_all_authors(self):
        c = self.connect()
        r = c.execute("SELECT name FROM authors")
        res = [x[0] for x in r.fetchall()]
        self.discard_and_disconnect()
        return res

    def get_all_authors_with_id(self):
        c = self.connect()
        r = c.execute("SELECT id, name FROM authors")
        res = [{"id":x[0],"name":x[1]} for x in r.fetchall()]
        self.discard_and_disconnect()
        return res

    def get_all_publications(self):
        c = self.connect()
        r = c.execute("SELECT name FROM publications")
        res = [x[0] for x in r.fetchall()]
        self.discard_and_disconnect()
        return res

    def get_all_publications_with_id(self):
        c = self.connect()
        r = c.execute("SELECT id, name FROM publications")
        res = [{"id":x[0],"name":x[1]} for x in r.fetchall()]
        self.discard_and_disconnect()
        return res

    def insert_tag(self,tag):
        c = self.connect()
        c.execute("INSERT INTO tags (name) VALUES (?)",(tag,))
        self.commit_and_disconnect()

    def delete_tag(self,tid):
        c = self.connect()
        c.execute("DELETE FROM tags WHERE id=?",(tid,))
        c.execute("DELETE FROM tp WHERE tid=?",(tid,))
        self.commit_and_disconnect()

    def insert_author(self,author):
        c = self.connect()
        c.execute("INSERT INTO authors (name) VALUES (?)",(author,))
        self.commit_and_disconnect()

    def delete_author(self,aid):
        c = self.connect()
        c.execute("DELETE FROM authors WHERE id=?",(aid,))
        c.execute("DELETE FROM ap WHERE aid=?",(aid,))
        self.commit_and_disconnect()
        
    def insert_publication(self,publication):
        c = self.connect()
        c.execute("INSERT INTO publications (name) VALUES (?)",(publication,))
        self.commit_and_disconnect()

    def delete_publication(self,pubid):
        c = self.connect()
        c.execute("DELETE FROM publications WHERE id=?",(pubid,))
        c.execute("DELETE FROM pubp WHERE pubid=?",(pubid,))
        self.commit_and_disconnect()
        
    # def get_paper(self,pid):
    #     req = "SELECT papers.title, "\
    #           "papers.id, "\
    #           "papers.path, "\
    #           "papers.notes, "\
    #           "papers.bibtex, "\
    #           "authors.name, "\
    #           "tags.name, "\
    #           "publications.name "\
    #           "FROM papers "\
    #           "INNER JOIN tp ON tp.pid = papers.id "\
    #           "INNER JOIN tags ON tags.id = tp.tid "\
    #           "INNER JOIN ap ON ap.pid = papers.id "\
    #           "INNER JOIN authors ON authors.id = ap.aid "\
    #           "INNER JOIN pubp ON pubp.pid = papers.id "\
    #           "INNER JOIN publications ON publications.id = pubp.pubid "\
    #           "WHERE papers.id = ?"
    #     c = self.connect()
    #     r = c.execute(req,(pid,))
    #     res = [x for x in r.fetchall()]
    #     self.discard_and_disconnect()
    #     paper = None
    #     for line in res:
    #         if not paper:
    #             paper = {"title":line[0],
    #                      "authors":[line[5]],
    #                      "publish":line[7],
    #                      "tags":[line[6]],
    #                      "notes":line[3],
    #                      "id":line[1],
    #                      "path":line[2],
    #                      "bibtex":line[4]}
    #         else:
    #             tag = line[6]
    #             author = line[5]
    #             if tag not in paper["tags"]:
    #                 paper["tags"].append(tag)
    #             if author not in paper["authors"]:
    #                 paper["authors"].append(author)
    #     return paper

    def get_paper(self,pid):
        paper = {"title":"","authors":[],"publish":"","tags":[],"notes":"","id":pid,"path":"","bibtex":""}
        c = self.connect()
        p = c.execute("SELECT title, path, notes, bibtex FROM papers WHERE id=?",(pid,)).fetchone()
        paper["title"] = p[0]
        paper["path"] = p[1]
        paper["notes"] = p[2]
        paper["bibtex"] = p[3]
        p = c.execute("SELECT name FROM publications WHERE id = (SELECT pubid FROM pubp WHERE pid = ?)",(pid,)).fetchone()
        if p:
            paper["publish"] = p[0]
        p = c.execute("SELECT name FROM tags INNER JOIN tp ON tp.tid = tags.id WHERE tp.pid = ?",(pid,)).fetchall()
        if p:
            for tag in p:
                if tag[0] not in paper["tags"]:
                    paper["tags"].append(tag[0])
        p = c.execute("SELECT name FROM authors INNER JOIN ap ON ap.aid = authors.id WHERE ap.pid = ?",(pid,)).fetchall()
        if p:
            for auth in p:
                if auth[0] not in paper["authors"]:
                    paper["authors"].append(auth[0])
        self.discard_and_disconnect()
        return paper
    
        
    def get_view(self,filt):
        req = "SELECT papers.title, papers.id FROM papers "
        tag_ids = []
        author_ids = []
        pub_ids = []
        args = []
        c = self.connect()
        if "tags" in filt.keys():
            for tag in filt["tags"]:
                res = c.execute("SELECT id FROM tags WHERE name=?",(tag,)).fetchone()
                if res:
                    tag_ids.append(res[0])
        if "authors" in filt.keys():
            for auth in filt["authors"]:
                res = c.execute("SELECT id FROM authors WHERE name=?",(auth,)).fetchone()
                if res:
                    author_ids.append(res[0])
        if "publish" in filt.keys():
            for pub in filt["publish"]:
                res = c.execute("SELECT id FROM publications WHERE name=?",(pub,)).fetchone()
                if res:
                    pub_ids.append(res[0])
        req_title = ""
        if "title" in filt.keys():
            req_title = req+" WHERE papers.title LIKE ? "
            args.append("%"+filt["title"]+"%")
        req_pub = ""
        if len(pub_ids)>0:
            req_pub = req+" INNER JOIN pubp ON pubp.pid = papers.id WHERE" 
            req_pub+=" ("+" OR ".join([" pubp.pubid = ? " for x in pub_ids])+") "
            args+=pub_ids
        req_author = ""
        if len(author_ids)>0:
            req_author = req+" INNER JOIN ap ON ap.pid = papers.id "
            if filt["authlogic"] == "or":
               req_author+= " WHERE "+ " ("+" OR ".join([" ap.aid = ? " for x in author_ids])+") "
            else:
                s = req_author+" WHERE ( ap.aid = ? ) "
                req_author = " INTERSECT ".join([s for x in author_ids])
            args+=author_ids
        req_tags = ""
        if len(tag_ids)>0:
            req_tags = req+" INNER JOIN tp ON tp.pid = papers.id "
            if filt["taglogic"] == "or":
               req_tags+= " WHERE "+ " ("+" OR ".join([" tp.tid = ? " for x in tag_ids])+") "
            else:
                s = req_tags+" WHERE ( tp.tid = ? ) "
                req_tags = " INTERSECT ".join([s for x in tag_ids])
            args+=tag_ids
        if req_author or req_tags or req_title or req_pub:
            req=" INTERSECT ".join(list(filter(lambda x:x!="",[req_title,req_pub,req_author,req_tags])))
        r = c.execute(req,tuple(args))
        res = [x for x in r.fetchall()]
        self.discard_and_disconnect()
        return View(res)
        
        
    def insert_paper(self,paper):
        c = self.connect()
        req = "INSERT INTO papers (title, path, notes, bibtex) VALUES (?,?,?,?)" 
        c.execute(req,(paper["title"],paper["path"],paper["notes"],paper["bibtex"]))
        res = c.execute("SELECT id FROM papers WHERE title=? AND path=?",(paper["title"],paper["path"])).fetchone()
        self.connection.commit()
        pid = res[0]
        #publication
        if paper["publish"]:
            res = c.execute("SELECT id FROM publications WHERE name=?",(paper["publish"],)).fetchone()
            pubid = res[0]
            c.execute("INSERT INTO pubp (pubid, pid) VALUES (?,?)",(pubid,pid))
        #tags
        for tag in paper["tags"]:
            res = c.execute("SELECT id FROM tags WHERE name=?",(tag,)).fetchone()
            tid = res[0]
            c.execute("INSERT INTO tp (tid, pid) VALUES (?,?)",(tid,pid))
        #authors
        for auth in paper["authors"]:
            res = c.execute("SELECT id FROM authors WHERE name=?",(auth,)).fetchone()
            aid = res[0]
            c.execute("INSERT INTO ap (aid, pid) VALUES (?,?)",(aid,pid))
        self.commit_and_disconnect()

    def update_paper(self,upd):
        c = self.connect()
        pid = upd["pid"]
        args = []
        req = "UPDATE papers SET "
        subreq = []
        if "title" in upd.keys():
            subreq.append(" title=? ")
            args.append(upd["title"])
        if "notes" in upd.keys():
            subreq.append(" notes=? ")
            args.append(upd["notes"])
        if "path" in upd.keys():
            subreq.append(" path=? ")
            args.append(upd["path"])
        if "bibtex" in upd.keys():
            subreq.append(" bibtex=? ")
            args.append(upd["bibtex"])
        if len(args) > 0:
            req+=",".join(subreq)+" WHERE id=?"
            args.append(pid)
            c.execute(req,tuple(args))
        if "publish" in upd.keys():
            res = c.execute("SELECT id FROM publications WHERE name=?",(upd["publish"],)).fetchone()
            pubid = res[0]
            res = c.execute("SELECT id FROM publications WHERE name=?",(upd["oldpublish"],)).fetchone()
            oldpubid = res[0]
            c.execute("INSERT INTO pubp (pubid, pid) VALUES (?,?)",(pubid,pid))
            c.execute("DELETE FROM pubp WHERE pubid=? AND pid=?",(oldpubid,pid))
        if "addtag" in upd.keys():
            for tag in upd["addtag"]:
                res = c.execute("SELECT id FROM tags WHERE name=?",(tag,)).fetchone()
                tid = res[0]
                c.execute("INSERT INTO tp (tid, pid) VALUES (?,?)",(tid,pid))
        if "rmtag" in upd.keys():
            for tag in upd["rmtag"]:
                res = c.execute("SELECT id FROM tags WHERE name=?",(tag,)).fetchone()
                tid = res[0]
                c.execute("DELETE FROM tp WHERE tid=? AND pid=?",(tid,pid))
        if "addauthor" in upd.keys():
            for author in upd["addauthor"]:
                res = c.execute("SELECT id FROM authors WHERE name=?",(author,)).fetchone()
                aid = res[0]
                c.execute("INSERT INTO ap (aid, pid) VALUES (?,?)",(aid,pid))
        if "rmauthor" in upd.keys():
            for author in upd["rmauthor"]:
                res = c.execute("SELECT id FROM authors WHERE name=?",(author,)).fetchone()
                aid = res[0]
                c.execute("DELETE FROM ap WHERE aid=? AND pid=?",(aid,pid))
        self.commit_and_disconnect()


    def delete_paper(self,pid):
        c = self.connect()
        c.execute("DELETE FROM papers WHERE id=?",(pid,))
        c.execute("DELETE FROM ap WHERE pid=?",(pid,))
        c.execute("DELETE FROM tp WHERE pid=?",(pid,))
        c.execute("DELETE FROM pubp WHERE pid=?",(pid,))
        self.commit_and_disconnect()


    def check_needed(self,eid,etype):
        c = self.connect()
        ans = True
        if etype == "tag":
            r = c.execute("SELECT pid FROM tp WHERE tid=?",(eid,)).fetchone()
            ans = (r != None)
        elif etype == "author":
            r = c.execute("SELECT pid FROM ap WHERE aid=?",(eid,)).fetchone()
            ans = (r != None)
        elif etype == "publication":
            r = c.execute("SELECT pid FROM pubp WHERE pubid=?",(eid,)).fetchone()
            ans = (r != None)
        self.discard_and_disconnect()
        return ans
