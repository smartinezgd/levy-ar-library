#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from levyar.database import Database
from levyar.configuration import Configuration
from levyar.ui.widgets import gen_info_dialog
import os
import subprocess
import shutil
import datetime
        
class Application(object):
    def __init__(self,cfg_path=None):
        self.configuration = Configuration()
        load_ans = "ok"
        if not cfg_path:
            cfg_path = os.path.join(os.environ["HOME"],".levyar/library.cfg")
            if os.path.exists(cfg_path):
                load_ans = self.configuration.load_config(cfg_path)
        else:
            load_ans = self.configuration.load_config(cfg_path)
        if load_ans != "ok":
            gen_info_dialog(None,"Error in Configuration file","An Error was met when loading configuration file\n"+cfg_path+" :\n"+load_ans+"\nA generic configuration will be used instead.")
            self.configuration = Configuration()
        self.load_database()
        self.set_view({})

    def load_database(self):
        self.database = Database(self.configuration.db_path)
        if not self.database.check_database():
            gen_info_dialog(None,"Wrong Database format","Configured database "+str(self.configuration.db_path)+" has a wrong schema. Please update it or change the configuration and restart Levy AR Library")

    def change_configuration(self,config):
        self.configuration = config
        self.load_database()
        self.view = self.database.get_view({})
        
    def get_all_tags(self,tid=False):
        if tid:
            return self.database.get_all_tags_with_id()
        else:
            return self.database.get_all_tags()


    def get_all_authors(self,aid=False):
        if aid:
            return self.database.get_all_authors_with_id()
        else:
            return self.database.get_all_authors()

    def get_all_publications(self,pubid=False):
        if pubid:
            return self.database.get_all_publications_with_id()
        else:
            return self.database.get_all_publications()

    def insert_tag(self,tag):
        return self.database.insert_tag(tag)

    def insert_publication(self,publication):
        return self.database.insert_publication(publication)

    def insert_author(self,author):
        return self.database.insert_author(author)

    def set_view(self,req):
        self.view = self.database.get_view(req)

    def get_paper(self,pid):
        return self.database.get_paper(pid)

    def open_paper(self,paper_path):
        if not os.path.exists(paper_path):
            return "Paper not found, "+paper_path+" could not be found"
        com = [paper_path if x == "%f" else x for x in self.configuration.pdf_command.split()]
        subprocess.Popen(com)
        return "ok"
        
    def insert_paper(self,paper):
        return self.database.insert_paper(paper)

    def update_paper(self,upd):
        return self.database.update_paper(upd)


    def copy_paper_file(self,path):
        if not path.startswith(self.configuration.papers_root):
            stamp = datetime.datetime.now().strftime("%d%m%Y%H%M%S")
            fname = stamp+os.path.basename(path)
            new_path = os.path.join(self.configuration.papers_root,fname)        
            shutil.copyfile(path,new_path)
            return new_path
        else:
            return path

    def delete_paper(self,pid,keep_pdf):
        if not keep_pdf:
            paper = self.get_paper(pid)
            path = paper["path"]
            if os.path.exists(path):
                os.remove(path)
        self.database.delete_paper(pid)
        self.set_view({})

    def delete_tag(self,tid):
        self.database.delete_tag(tid)

    def delete_author(self,aid):
        self.database.delete_author(aid)

    def delete_publication(self,pubid):
        self.database.delete_publication(pubid)

    def check_needed(self,eid,etype):
        return self.database.check_needed(eid,etype)
