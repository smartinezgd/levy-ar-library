#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt5.QtWidgets import QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QPushButton, QFileDialog, QLineEdit
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,pyqtSignal
from levyar.ui.widgets import gen_yes_no_dialog,gen_info_dialog
from levyar.ui.icons import get_icons_path
from levyar.configuration import Configuration
import os

icons = get_icons_path()

class ConfWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.title = "Configuration editor"
        self.left = 100
        self.top = 80
        self.width = 500
        self.height = 350
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = ConfCentral(self)
        self.setCentralWidget(self.central)

    def close_w(self):
        self.close_sig.emit()
        self.close()

    def update(self):
        self.central.get_app_config()
        self.central.update()
        
        
class ConfCentral(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.win = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.config = Configuration()
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setAlignment(Qt.AlignCenter)
        self.head.setFixedHeight(20)
        self.head.setText("Select configuration parameters")
        self.layout.addWidget(self.head)
        self.openbtns = QWidget(self)
        self.openbtns.layout = QHBoxLayout()
        self.openbtns.layout.addStretch(1)
        self.openbtns.label = QLabel(self)
        self.openbtns.label.setFixedHeight(15)
        self.openbtns.label.setText("Open configuration file")
        self.openbtns.layout.addWidget(self.openbtns.label)
        self.openbtns.btn = QPushButton(self)
        self.openbtns.btn.setText("Open")
        self.openbtns.btn.clicked.connect(self.open_conf)
        self.openbtns.layout.addWidget(self.openbtns.btn)
        self.openbtns.setLayout(self.openbtns.layout)
        self.layout.addWidget(self.openbtns)
        self.blank = QLabel(self)
        self.blank.setFixedHeight(8)
        self.layout.addWidget(self.blank)
        self.confpath = QLabel(self)
        self.confpath.setFont(self.bold)
        self.confpath.setAlignment(Qt.AlignCenter)
        self.confpath.setFixedHeight(20)
        self.confpath.setText("Configuration file")
        self.layout.addWidget(self.confpath)
        self.confpathbtn = QPushButton(self)
        self.confpathbtn.setText("Select configuration file")
        self.confpathbtn.clicked.connect(self.click_confpath)
        self.confpathbtn.setFixedHeight(25)
        self.layout.addWidget(self.confpathbtn)
        self.dbpath = QLabel(self)
        self.dbpath.setFont(self.bold)
        self.dbpath.setAlignment(Qt.AlignCenter)
        self.dbpath.setFixedHeight(20)
        self.dbpath.setText("Database file")
        self.layout.addWidget(self.dbpath)
        self.dbpathbtn = QPushButton(self)
        self.dbpathbtn.setText("Select database")
        self.dbpathbtn.clicked.connect(self.click_dbpath)
        self.dbpathbtn.setFixedHeight(25)
        self.layout.addWidget(self.dbpathbtn)
        self.paperpath = QLabel(self)
        self.paperpath.setFont(self.bold)
        self.paperpath.setAlignment(Qt.AlignCenter)
        self.paperpath.setFixedHeight(20)
        self.paperpath.setText("Paper storage root")
        self.layout.addWidget(self.paperpath)
        self.paperpathbtn = QPushButton(self)
        self.paperpathbtn.setText("Select paper storage root")
        self.paperpathbtn.clicked.connect(self.click_paperpath)
        self.paperpathbtn.setFixedHeight(25)
        self.layout.addWidget(self.paperpathbtn)
        self.command = QLabel(self)
        self.command.setFont(self.bold)
        self.command.setAlignment(Qt.AlignCenter)
        self.command.setFixedHeight(20)
        self.command.setText("PDF reader command")
        self.layout.addWidget(self.command)
        self.commandwdg = QWidget(self)
        self.commandwdg.layout = QHBoxLayout()
        self.commandwdg.layout.addStretch(1)
        self.commandinp = QLineEdit(self.commandwdg)
        self.commandinp.setText("command %f")
        self.commandinp.textChanged.connect(self.change_command)
        self.commandinp.setFixedHeight(20)
        self.commandinp.setFixedWidth(250)
        self.commandinp.setAlignment(Qt.AlignCenter)
        self.commandwdg.layout.addWidget(self.commandinp)
        self.commandwdg.layout.addStretch(1)
        self.commandwdg.setLayout(self.commandwdg.layout)
        self.layout.addWidget(self.commandwdg)
        self.blank2 = QLabel(self)
        self.blank2.setFixedHeight(15)
        self.layout.addWidget(self.blank2)
        #Buttons
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.validatebtn = QPushButton(self.buttons)
        self.validatebtn.setText("Validate")
        self.validatebtn.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.validatebtn)
        self.cancelbtn = QPushButton(self.buttons)
        self.cancelbtn.setText("Cancel")
        self.cancelbtn.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.cancelbtn)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)
        self.layout.addStretch(1)
        self.setLayout(self.layout)


    def click_confpath(self):
        path = QFileDialog.getSaveFileName(self, 'Save configuration file as', '.',filter=('YaML (*.yaml)'))[0]
        if path:
            self.config.file_path = os.path.abspath(path)
            self.confpathbtn.setText(self.short_string(self.config.file_path))
            self.config.virtual = False

    def click_dbpath(self):
        path = QFileDialog.getSaveFileName(self, 'Write databse in', '.',filter=('SQLite3 database (*.db)'))[0]
        if path:
            self.config.db_path = os.path.abspath(path)
            self.dbpathbtn.setText(self.short_string(self.config.db_path))
            self.config.virtual = False

    def click_paperpath(self):
        path = QFileDialog.getExistingDirectory(self, 'Save papers in', '.')
        if path and os.path.exists(path):
            self.config.papers_root = os.path.abspath(path)
            self.paperpathbtn.setText(self.short_string(self.config.papers_root))
            self.config.virtual = False

    def open_conf(self):
        path = QFileDialog.getOpenFileName(self, 'Select configuration file', '.',filter=('YaML (*.yaml)'))[0]
        if path and os.path.exists(path):
            conf = Configuration()
            res = conf.load_config(path)
            if res != "ok":
                gen_info_dialog("Error","An error occured when opening the configuration file :\n"+res)
            else:
                self.config = conf
                self.update()

    def change_command(self):
        self.config.pdf_command = self.commandinp.text().strip()
                
    def get_app_config(self):
        self.config = self.win.app.configuration.clone()

    def short_string(self,s):
        if len(s) > 50:
            return s[0:47]+"..."
        else:
            return s
        
    def update(self):
        self.confpathbtn.setText(self.short_string(self.config.file_path))
        self.dbpathbtn.setText(self.short_string(self.config.db_path))
        self.paperpathbtn.setText(self.short_string(self.config.papers_root))
        self.commandinp.setText(self.config.pdf_command)

    def cancel(self):
        self.win.close_w()

    def validate(self):
        if self.config.validate() == "ok":
            self.config.save_config()
            self.win.app.change_configuration(self.config)
        self.win.close_w()
    
