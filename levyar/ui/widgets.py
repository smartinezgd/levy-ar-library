#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMessageBox
import os

def gen_quit_dialog(parent):
    msgBox = QMessageBox(parent)
    msgBox.setWindowTitle("Quit without saving?")
    msgBox.setText("Configuration was not saved\n\nDo you want to save?")
    msgBox.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
    msgBox.setDefaultButton(QMessageBox.Cancel)
    reply = msgBox.exec_()
    if reply == QMessageBox.Save:
        return "save"
    elif reply == QMessageBox.Discard:
        return "discard"
    else:
        return "cancel"

def gen_info_dialog(parent,title,message):
    msgBox = QMessageBox(parent)
    msgBox.setWindowTitle(title)
    msgBox.setText(message)
    msgBox.setStandardButtons(QMessageBox.Ok)
    msgBox.setDefaultButton(QMessageBox.Ok)
    reply = msgBox.exec_()


def gen_yes_no_dialog(parent,title,message):
    msgBox = QMessageBox(parent)
    msgBox.setWindowTitle(title)
    msgBox.setText(message)
    msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
    msgBox.setDefaultButton(QMessageBox.No)
    reply = msgBox.exec_()
    return reply == QMessageBox.Yes

def gen_yes_no_cancel_dialog(parent,title,message):
    msgBox = QMessageBox(parent)
    msgBox.setWindowTitle(title)
    msgBox.setText(message)
    msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
    msgBox.setDefaultButton(QMessageBox.Cancel)
    reply = msgBox.exec_()
    if reply == QMessageBox.Yes:
        return "yes"
    elif reply == QMessageBox.No:
        return "no"
    else:
        return "cancel"

