#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel, QListWidget, QListWidgetItem, QPushButton, QSizePolicy,QTextEdit, QLineEdit
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,QSize
from levyar.ui.icons import get_icons_path
from levyar.ui.widgets import gen_info_dialog
import os

icons = get_icons_path()


class PapersView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.app = parent.app
        self.topLayout = QVBoxLayout()
        self.top = TopWidget(self)
        self.topLayout.addWidget(self.top)
        self.blank = QWidget(self)
        self.blank.setFixedHeight(10)
        self.topLayout.addWidget(self.blank)
        self.mainwidget = QWidget(self)
        self.topLayout.addWidget(self.mainwidget)
        self.layout = QHBoxLayout()
        self.paper = Paper(self)
        self.pannel = Pannel(self)
        self.layout.addWidget(self.paper)
        self.layout.addWidget(self.pannel)
        self.mainwidget.setLayout(self.layout)
        self.setLayout(self.topLayout)

    def clear(self):
        self.paper.clear()
        self.pannel.clear()
        if not self.central.searched:
            self.blank.show()
            self.top.hide()

    def update(self):
        self.paper.update()

    def setTop(self,txt):
        self.blank.hide()
        self.top.update(txt)
        self.top.show()

    def edit_paper(self,paper):
        self.central.mainwin.edit_paper(paper)

    def look_paper(self,pid):
        self.pannel.paper = self.app.get_paper(pid)
        self.pannel.update()

    def open_paper(self,paper_path):
        ans = self.app.open_paper(paper_path)
        if ans != "ok":
            gen_info_dialog(self.central.mainwin,"Error when opening paper","An errer was met when opening paper:\n"+ans)
        
    def clean(self):
        self.top.lbl.setText("") 
        self.top.lbl.setFixedHeight(35)
        self.pannel.paper = None
        self.clear()
        
            
class TopWidget(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.sizePolicy = QSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.layout = QHBoxLayout()
        self.lbl = QLabel(self)
#        self.lbl.setFont(self.bold)
        self.lbl.setFixedHeight(35)
        self.lbl.sizePolicy = QSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.lbl.sizePolicy.setHorizontalStretch(1)
        self.lbl.setSizePolicy(self.lbl.sizePolicy)
        self.layout.addWidget(self.lbl)
        self.button = QPushButton(self)
        self.button.setFixedHeight(20)
        self.button.setText("Reset")
        self.button.clicked.connect(self.on_reset)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

    def update(self,txt):
        l = txt.strip().split()
        sl = []
        n = 0
        line = []
        for w in l:
            if len(w)+n+1 <=180:
                line.append(w)
                n+=len(w)+1
            else:
                sl.append(" ".join(line))
                line=[w]
                n = len(w)
        if len(line) >0:
            sl.append(" ".join(line))
        self.lbl.setFixedHeight(max(35,15*len(sl)))
        self.lbl.setText("\n                       ".join(sl)) 

    def on_reset(self):
        self.view.central.searched = False
        self.view.central.global_refresh()
        self.view.update()
                           
class Paper(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.sizePolicy = QSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.sizePolicy.setHorizontalStretch(2)
        self.setSizePolicy(self.sizePolicy)
        self.layout = QVBoxLayout()
        self.lbl_title = QLabel(self)
        self.lbl_title.setFont(self.bold)
        self.lbl_title.setText("Papers")
        self.layout.addWidget(self.lbl_title)
        self.paperlist = QListWidget(self)
        self.layout.addWidget(self.paperlist)
        self.setLayout(self.layout)
        self.paperlist.itemClicked.connect(self.onItemClicked)
        self.update()

    def onItemClicked(self, item):
        self.view.look_paper(item.paper_id)

    def update(self):
        for paper in self.view.app.view.papers:
            item = QListWidgetItem(paper["title"])
            item.paper_id = paper["id"]
            self.paperlist.addItem(item)

    def clear(self):
        self.paperlist.clear()
            

class Pannel(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.sizePolicy = QSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.sizePolicy.setHorizontalStretch(1)
        self.setSizePolicy(self.sizePolicy)
        self.layout = QVBoxLayout()
        self.blank = QWidget(self)
        self.blank.setFixedHeight(25)
        self.layout.addWidget(self.blank)
        self.paper_title = QLineEdit(self)
        self.paper_title.setFont(self.bold)
        self.paper_title.setAlignment(Qt.AlignCenter)
        self.paper_title.setFixedHeight(25)
        self.paper_title.setReadOnly(True)
        self.layout.addWidget(self.paper_title)
        #Buttons
        self.buttons = QWidget(self)
        self.buttonlayout = QHBoxLayout()
        self.openbtn = QPushButton(self.buttons)
        self.openbtn.setText("Open paper")
        self.openbtn.clicked.connect(self.openPaper)
        self.buttonlayout.addWidget(self.openbtn)
        self.editbtn = QPushButton(self.buttons)
        self.editbtn.setText("Edit paper")
        self.editbtn.clicked.connect(self.editPaper)
        self.buttonlayout.addWidget(self.editbtn)
        self.buttons.setLayout(self.buttonlayout)
        self.layout.addWidget(self.buttons)
        #Paper content
        self.authors = Authors(self)
        self.layout.addWidget(self.authors)
        self.publish = Publisher(self)
        self.layout.addWidget(self.publish)
        self.tags = Tags(self)
        self.layout.addWidget(self.tags)
        self.notes = Notes(self)
        self.layout.addWidget(self.notes)
        self.bibtex = BibTex(self)
        self.layout.addWidget(self.bibtex)
        self.layout.addStretch(1)
        self.setLayout(self.layout)
        self.paper = None

    def update(self):
        if self.paper:
            self.paper_title.setText(self.paper["title"])
            self.authors.update(self.paper["authors"])
            self.publish.update(self.paper["publish"])
            self.tags.update(self.paper["tags"])
            self.notes.update(self.paper["notes"])
            self.bibtex.update(self.paper["bibtex"])

    def clear(self):
        self.paper_title.setText("")
        self.authors.clear()
        self.publish.clear()
        self.tags.clear()
        self.notes.clear()
        self.bibtex.clear()

    def openPaper(self):
        if self.paper:
            self.view.open_paper(self.paper["path"])

    def editPaper(self):
        if self.paper:
            self.view.edit_paper(self.paper)
  

class Authors(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.pannel = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("By")
        self.head.setFixedWidth(25)
        self.layout.addWidget(self.head)
        self.authors = QTextEdit(self)
        self.authors.setFixedHeight(50)
        self.authors.setReadOnly(True)
        self.authors.setAlignment(Qt.AlignLeft)
        self.layout.addWidget(self.authors)
        self.setLayout(self.layout)

    def update(self,authors):
        self.authors.setFixedHeight(max(50,20*len(authors)))
        s = "\n".join([a for a in authors])
        self.authors.setPlainText(s)
            
    def clear(self):
        self.authors.setPlainText("")
        self.authors.setFixedHeight(50)

class Publisher(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.pannel = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("Published in")
        self.head.setFixedHeight(20)
        self.head.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.head)
        self.pub = QTextEdit(self)
        self.pub.setAlignment(Qt.AlignCenter)
        self.pub.setFixedHeight(50)
        self.pub.setReadOnly(True)
        self.layout.addWidget(self.pub)
        self.setLayout(self.layout)
        
    def update(self,publish):
        l = publish.split("\n")
        sl = []
        for x in l:
            x0 = x.strip()
            if len(x0)>50:
                x0l = x0.split()
                n = 0
                s = []
                for w in x0l:
                    if len(w)+n+1 <= 50:
                        s.append(w)
                        n+=len(w)+1
                    else:
                        sl.append(" ".join(s))
                        s=[w]
                        n = len(w)
                if len(s) >0:
                    sl.append(" ".join(s))
            else:
                sl.append(x0)
        self.pub.setFixedHeight(max(50,20*len(sl)))
        self.pub.setPlainText("\n".join(sl))

    def clear(self):
        self.pub.setPlainText("")
        self.pub.setFixedHeight(50)

        
class Tags(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.pannel = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("Tags")
        self.head.setFixedHeight(20)
        self.head.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.head)
        self.tags = QLabel(self)
        self.tags.setFixedHeight(40)
        self.tags.setStyleSheet("QLabel {background-color : white;}");
        self.tags.setAlignment(Qt.AlignLeft)
        self.layout.addWidget(self.tags)
        self.setLayout(self.layout)

    def update(self,tags):
        sl = []
        n = 0
        s = []
        for t in tags:
            if len(t)+n+1 <= 50:
                s.append(t)
                n+=len(t)+1
            else:
                sl.append(", ".join(s))
                s=[t]
                n = len(t)
        if len(s) > 0:
            sl.append(" ".join(s))
        self.tags.setFixedHeight(max(40,20*len(sl)))
        self.tags.setText("\n".join(sl))

    def clear(self):
        self.tags.setText("")
        self.tags.setFixedHeight(40)

class Notes(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.pannel = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("Notes")
        self.head.setFixedHeight(20)
        self.head.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.head)
        self.notes = QTextEdit(self)
        self.notes.setFixedHeight(90)
        self.notes.setReadOnly(True)
        self.notes.setAlignment(Qt.AlignLeft)
        self.layout.addWidget(self.notes)
        self.setLayout(self.layout)

    def update(self,notes):
        l = notes.split("\n")
        sl = []
        for x in l:
            x0 = x.strip()
            if len(x0)>50:
                x0l = x0.split()
                n = 0
                s = []
                for w in x0l:
                    if len(w)+n+1 <= 50:
                        s.append(w)
                        n+=len(w)+1
                    else:
                        sl.append(" ".join(s))
                        s=[w]
                        n = len(w)
                if len(s) >0:
                    sl.append(" ".join(s))
            else:
                sl.append(x0)
        self.notes.setFixedHeight(max(90,20*len(sl)))
        self.notes.setPlainText("\n".join(sl))

    def clear(self):
        self.notes.setPlainText("")
        self.notes.setFixedHeight(90)

class BibTex(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.pannel = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("BibTeX")
        self.head.setFixedHeight(20)
        self.head.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.head)
        self.bib = QTextEdit(self)
        self.bib.setFixedHeight(70)
        self.bib.setAlignment(Qt.AlignLeft)
        self.bib.setReadOnly(True)
        self.layout.addWidget(self.bib)
        self.setLayout(self.layout)

    def update(self,bib):
        l = bib.split("\n")
        sl = []
        for x in l:
            x0 = x.strip()
            if len(x0)>50:
                x0l = x0.split()
                n = 0
                s = []
                for w in x0l:
                    if len(w)+n+1 <= 50:
                        s.append(w)
                        n+=len(w)+1
                    else:
                        sl.append(" ".join(s))
                        s=[w]
                        n = len(w)
                if len(s) >0:
                    sl.append(" ".join(s))
            else:
                sl.append(x0)
        self.bib.setFixedHeight(max(70,20*len(sl)))
        self.bib.setPlainText("\n".join(sl))

    def clear(self):
        self.bib.setPlainText("")
        self.bib.setFixedHeight(70)
