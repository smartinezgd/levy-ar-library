#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel, QGroupBox, QPushButton, QLineEdit,QTextEdit, QComboBox, QAbstractItemView, QListWidget, QListWidgetItem
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,QSize
from levyar.ui.icons import get_icons_path
import os

icons = get_icons_path()


class SearchView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.query = {}
        self.layout = QVBoxLayout()
        self.blank0 = QWidget(self)
        self.blank0.setFixedHeight(40)
        self.layout.addWidget(self.blank0)
        self.header = QWidget(self)
        self.header.layout = QHBoxLayout()
        self.header.layout.addStretch(1)
        self.header.label = QLabel(self)
        self.header.label.setFont(self.bold)
        self.header.label.setText("Search the database for papers ...")
        self.header.layout.addWidget(self.header.label)
        self.header.blank = QWidget(self)
        self.header.blank.setFixedWidth(80)
        self.header.layout.addWidget(self.header.blank)
        self.header.button = QPushButton(self)
        self.header.button.setFixedHeight(50)
        self.header.button.setFixedWidth(70)
        self.header.button.setText("Search")
        self.header.button.clicked.connect(self.search)
        self.header.layout.addWidget(self.header.button)
        self.header.layout.addStretch(1)
        self.header.setLayout(self.header.layout)
        self.layout.addWidget(self.header)
        self.blank = QWidget(self)
        self.blank.setFixedHeight(50)
        self.layout.addWidget(self.blank)
        self.middle = QWidget(self)
        self.middle.layout = QHBoxLayout()
        self.box1 = QWidget(self)
        self.box1.layout = QVBoxLayout()
        self.titleInput = TitleInput(self)
        self.box1.layout.addWidget(self.titleInput)
        self.pubInput = PublishInput(self)
        self.box1.layout.addWidget(self.pubInput)
        self.box1.layout.addStretch(1)
        self.box1.setLayout(self.box1.layout)
        self.middle.layout.addWidget(self.box1)
        self.box2 = QWidget(self)
        self.box2.layout = QHBoxLayout()
        self.box2.layout.addStretch(1)
        self.authorInput = AuthorInput(self)
        self.box2.layout.addWidget(self.authorInput)
        self.tagInput = TagInput(self)
        self.box2.layout.addWidget(self.tagInput)
        self.box2.layout.addStretch(1)
        self.box2.setLayout(self.box2.layout)
        self.middle.layout.addWidget(self.box2)
        self.middle.setLayout(self.middle.layout)
        self.layout.addWidget(self.middle)
        self.layout.addStretch(1)
        self.setLayout(self.layout)

    def clear(self):
        if self.query == {}:
            self.titleInput.clear()
            self.pubInput.clear()
            self.tagInput.clear()
            self.authorInput.clear()

    def update(self):
        self.titleInput.update()
        self.pubInput.update()
        self.tagInput.update()
        self.authorInput.update()

    def reset_query(self):
        self.query = {}
        self.clear()
        
    def search(self):
        self.app.set_view(self.query)
        self.central.gotoTab(0)
        self.central.setTop("Showing results of query : "+self.query_to_string())

    def query_to_string(self):
        s = ""
        if "title" in self.query.keys():
            s+="title matches '"+self.query["title"]+"', "
        if "publish" in self.query.keys():
            s+="published in '"+"' or '".join([x.split("\n")[0] for x in self.query["publish"]])+"', "
        if "authors" in self.query.keys():
            s+="written by "
            if self.query["authlogic"] == "and":
                s+=" and ".join(self.query["authors"])+", "
            else:
                s+=" or ".join(self.query["authors"])+", "
        if "tags" in self.query.keys():
            s+="tagged with "
            if self.query["taglogic"] == "and":
                s+=" and ".join(self.query["tags"])
            else:
                s+=" or ".join(self.query["tags"])
        if not s:
            s = "empty query"
        return s.strip().rstrip(",")

        
class TitleInput(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_paper = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("... which title contains the following")
        self.box.layout = QVBoxLayout()
        self.titleinput = QLineEdit(self)
        self.titleinput.setFixedHeight(30)
        self.titleinput.setFixedWidth(600)
        self.titleinput.textChanged.connect(self.on_title_change)
        self.box.layout.addWidget(self.titleinput)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)

    def update(self):
        if "title" in self.view.query.keys():
            self.titleinput.setText(self.view.query["title"])

    def clear(self):
        self.titleinput.setText("")

    def on_title_change(self):
        if self.titleinput.text().strip():
            self.view.query["title"] = self.titleinput.text().strip()


class PublishInput(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_paper = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("... published in one of the selected media")
        self.box.layout = QVBoxLayout()
        self.tlist = QListWidget(self)
        self.tlist.itemClicked.connect(self.onItemClicked)
        self.tlist.setSelectionMode(QAbstractItemView.MultiSelection)
        self.tlist.setFixedWidth(600)
        self.tlist.setFixedHeight(350)
        self.box.layout.addWidget(self.tlist)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)

    def update(self):
        if "publish" in self.view.query.keys():
            for pub in self.view.query["publish"]:
                for i in range(self.tlist.count()):
                    if self.tlist.item(i).text() == pub:
                        self.tlist.item(i).setSelected(True)
                        break

    def clear(self):
        all_publications = self.view.app.get_all_publications()
        self.tlist.clear()
        for pub in all_publications:
            self.tlist.addItem(QListWidgetItem(pub))

    def onItemClicked(self,item):
        pub = item.text()
        if "publish" in self.view.query.keys():
            if pub in self.view.query["publish"]:
                self.view.query["publish"].remove(pub)
            else:
                self.view.query["publish"].append(pub)
        else:
            self.view.query["publish"] = [pub]

            
class TagInput(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_paper = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("... tagged with")
        self.box.layout = QVBoxLayout()
        self.logic = QComboBox(self)
        self.logic.addItems(["all selected tags","at least one of the selected tags"])
        self.logic.setFixedWidth(250)
        self.logic.setFixedHeight(30)
        self.logic.currentIndexChanged.connect(self.on_logic_select)
        self.box.layout.addWidget(self.logic)
        self.tlist = QListWidget(self)
        self.tlist.itemClicked.connect(self.onItemClicked)
        self.tlist.setSelectionMode(QAbstractItemView.MultiSelection)
        self.tlist.setFixedHeight(400)
        self.box.layout.addWidget(self.tlist)
        self.logic.setCurrentIndex(0)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)

    def update(self):
        if "tags" in self.view.query.keys():
            for tag in self.view.query["tags"]:
                for i in range(self.tlist.count()):
                    if self.tlist.item(i).text() == tag:
                        self.tlist.item(i).setSelected(True)
                        break
        if "taglogic" in self.view.query.keys():
            if self.view.query["taglogic"] == "and":
                self.logic.setCurrentIndex(0)
            else:
                self.logic.setCurrentIndex(1)
        else:
            self.logic.setCurrentIndex(0)
            self.view.query["taglogic"] = "and"

    def clear(self):
        all_tags = self.view.app.get_all_tags()
        self.tlist.clear()
        self.logic.setCurrentIndex(0)
        for tag in all_tags:
            self.tlist.addItem(QListWidgetItem(tag))

    def onItemClicked(self,item):
        tag = item.text()
        if "tags" in self.view.query.keys():
            if tag in self.view.query["tags"]:
                self.view.query["tags"].remove(tag)
            else:
                self.view.query["tags"].append(tag)
        else:
            self.view.query["tags"] = [tag]

    def on_logic_select(self,i):
        if i == 0:
            self.view.query["taglogic"] = "and"
        else:
            self.view.query["taglogic"] = "or"


class AuthorInput(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_paper = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("... written by")
        self.box.layout = QVBoxLayout()
        self.logic = QComboBox(self)
        self.logic.addItems(["all selected authors","at least one of the selected authors"])
        self.logic.setFixedWidth(270)
        self.logic.setFixedHeight(30)
        self.logic.currentIndexChanged.connect(self.on_logic_select)
        self.box.layout.addWidget(self.logic)
        self.alist = QListWidget(self)
        self.alist.itemClicked.connect(self.onItemClicked)
        self.alist.setSelectionMode(QAbstractItemView.MultiSelection)
        self.alist.setFixedHeight(400)
        self.alist.setFixedWidth(350)
        self.box.layout.addWidget(self.alist)
        self.logic.setCurrentIndex(0)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)

    def update(self):
        if "authors" in self.view.query.keys():
            for auth in self.view.query["authors"]:
                for i in range(self.alist.count()):
                    if self.alist.item(i).text() == auth:
                        self.alist.item(i).setSelected(True)
                        break
        if "authlogic" in self.view.query.keys():
            if self.view.query["authlogic"] == "and":
                self.logic.setCurrentIndex(0)
            else:
                self.logic.setCurrentIndex(1)
        else:
            self.logic.setCurrentIndex(0)
            self.view.query["authlogic"] = "and"

    def clear(self):
        all_authors = self.view.app.get_all_authors()
        self.alist.clear()
        self.logic.setCurrentIndex(0)
        for auth in all_authors:
            self.alist.addItem(QListWidgetItem(auth))

    def onItemClicked(self,item):
        auth = item.text()
        if "authors" in self.view.query.keys():
            if auth in self.view.query["authors"]:
                self.view.query["authors"].remove(auth)
            else:
                self.view.query["authors"].append(auth)
        else:
            self.view.query["authors"] = [auth]

    def on_logic_select(self,i):
        if i == 0:
            self.view.query["authlogic"] = "and"
        else:
            self.view.query["authlogic"] = "or"
