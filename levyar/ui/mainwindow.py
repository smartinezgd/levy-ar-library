#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt5.QtWidgets import QMainWindow,QAction,qApp,QMessageBox,QFileDialog
from PyQt5.QtGui import QIcon
from levyar.ui.widgets import gen_quit_dialog,gen_info_dialog,gen_yes_no_dialog
from levyar.ui.icons import get_icons_path
from levyar.ui.central import CentralView
from levyar.ui.addpaper import AddPaperWindow
from levyar.ui.conf import ConfWindow
import os

icons = get_icons_path()

class MainWindow(QMainWindow):
    def __init__(self,application):
        super().__init__()
        self.title = "Levy Advance Reseach Library"
        self.app = application
        self.left = 40
        self.top = 40
        self.width = 1440
        self.height = 900
        self.setWindowTitle(self.title)
        self.setFixedSize(self.width,self.height)
        self.create_toolbar() #Init toolbar
        self.central_view = CentralView(self)
        self.setCentralWidget(self.central_view)
        self.addPaper = AddPaperWindow(self)
        self.addPaper.close_sig.connect(self.onAddPaperClose)
        self.confWin = ConfWindow(self)
        self.confWin.close_sig.connect(self.onConfClose)
        self.central_view.clear()
        self.central_view.update()
        self.show()

    def create_toolbar(self):
        ###########
        # Actions #
        ###########
        #Generic
        exitA = QAction(QIcon(os.path.join(icons,"quit.png")),"Quit",self)
        exitA.triggered.connect(self.quit_method)
        newA = QAction(QIcon(os.path.join(icons,"new.png")),"Add new paper",self)
        newA.triggered.connect(self.new_paper)
        confA = QAction(QIcon(os.path.join(icons,"write.png")),"Edit configuration",self)
        confA.triggered.connect(self.open_config)
        #Menu
        self.menubar = self.menuBar()
        papers = self.menubar.addMenu("Papers")
        papers.addAction(newA)
        conf = self.menubar.addMenu("Configuration")
        conf.addAction(confA)
        lev = self.menubar.addMenu("Levy AR Library")
        lev.addAction(exitA)

    def new_paper(self):
        self.addPaper.new_paper()
        self.addPaper.show()

    def edit_paper(self,paper):
        self.addPaper.edit(paper)
        self.addPaper.show()

    def onAddPaperClose(self):
        self.central_view.global_refresh()
        self.central_view.papersview.update()

    def onConfClose(self):
        pass

    def total_update(self):
        self.central_view.update()

    def open_config(self):
        self.confWin.update()
        self.confWin.show()

    def quit_method(self):
        qApp.quit()

    def closeEvent(self,event):
        self.quit_method()

  
