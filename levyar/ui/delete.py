#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel,QGroupBox, QPushButton,QListWidget,QListWidgetItem
from PyQt5.QtCore import Qt,QSize
from PyQt5.QtGui import QFont
from levyar.ui.icons import get_icons_path
from levyar.ui.widgets import gen_yes_no_cancel_dialog, gen_yes_no_dialog
from levyar.ui.papers import TopWidget
import os

icons = get_icons_path()


class DeleteView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.central = parent
        self.layout = QVBoxLayout()
        self.top = TopWidget(self)
        self.layout.addWidget(self.top)
        self.blank = QWidget(self)
        self.blank.setFixedHeight(10)
        self.layout.addWidget(self.blank)
        self.mainwidget = QWidget(self)
        self.mainwidget.layout = QHBoxLayout()
        self.paperlist = PaperList(self)
        self.mainwidget.layout.addWidget(self.paperlist)
        self.secondary = QWidget(self)
        self.secondary.layout = QVBoxLayout()
        self.taglist = TagList(self)
        self.secondary.layout.addWidget(self.taglist)
        self.authorlist = AuthorList(self)
        self.secondary.layout.addWidget(self.authorlist)
        self.publist = PublicationList(self)
        self.secondary.layout.addWidget(self.publist)
        self.secondary.setLayout(self.secondary.layout)
        self.mainwidget.layout.addWidget(self.secondary)
        self.mainwidget.setLayout(self.mainwidget.layout)
        self.layout.addWidget(self.mainwidget)
        self.setLayout(self.layout)

    def clear(self):
        self.paperlist.clear()
        self.publist.clear()
        self.authorlist.clear()
        self.taglist.clear()
        if not self.central.searched:
            self.blank.show()
            self.top.hide()

    def update(self):
        self.paperlist.update()
        self.publist.update()
        self.authorlist.update()
        self.taglist.update()

    def global_refresh(self):
        self.central.global_refresh()

    def setTop(self,txt):
        self.blank.hide()
        self.top.update(txt)
        self.top.show()

    def clean(self):
        self.top.lbl.setText("") 
        self.top.lbl.setFixedHeight(35)
        self.clear()

class PaperList(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_paper = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("Delete one paper")
        self.box.layout = QVBoxLayout()
        self.paperlist = QListWidget(self)
        self.box.layout.addWidget(self.paperlist)
        self.button = QPushButton(self)
        self.button.setText("Delete Paper")
        self.button.clicked.connect(self.on_delete)
        self.box.layout.addWidget(self.button)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)
        self.paperlist.itemClicked.connect(self.onItemClicked)
        self.update()

    def onItemClicked(self, item):
        self.current_paper = item.paper_id

    def update(self):
        for paper in self.view.app.view.papers:
            item = QListWidgetItem(paper["title"])
            item.paper_id = paper["id"]
            self.paperlist.addItem(item)

    def clear(self):
        self.paperlist.clear()

    def on_delete(self):
        if self.current_paper != -1:
            ans = gen_yes_no_cancel_dialog(self.view.central.mainwin,"Delete paper","Do you wih to keep the PDF file?")
            if ans == "yes":
                self.view.app.delete_paper(self.current_paper,True)
            elif ans == "no":
                self.view.app.delete_paper(self.current_paper,False)
            if ans != "cancel":
                self.clear()
                self.update()
    
class TagList(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_tag = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("Delete one tag")
        self.box.layout = QVBoxLayout()
        self.tlist = QListWidget(self)
        self.box.layout.addWidget(self.tlist)
        self.button = QPushButton(self)
        self.button.setText("Delete tag")
        self.button.clicked.connect(self.on_delete)
        self.box.layout.addWidget(self.button)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)
        self.tlist.itemClicked.connect(self.onItemClicked)
        self.update()

    def onItemClicked(self, item):
        self.current_tag = item.tag_id

    def update(self):
        all_tags = self.view.app.get_all_tags(tid=True)
        for tag in all_tags:
            item = QListWidgetItem(tag["name"])
            item.tag_id = tag["id"]
            self.tlist.addItem(item)

    def clear(self):
        self.tlist.clear()

    def on_delete(self):
        if self.current_tag != -1:
            todel = True
            if self.view.app.check_needed(self.current_tag,"tag"):
                if not gen_yes_no_dialog(self.view.central.mainwin,"Delete tag","Selected tag is used by at least one paper\nDelete it anyway?"):
                    todel = False
            if todel:
                self.view.app.delete_tag(self.current_tag)
                self.clear()
                self.update()
                

    
class AuthorList(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_author = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("Delete one author")
        self.box.layout = QVBoxLayout()
        self.tlist = QListWidget(self)
        self.box.layout.addWidget(self.tlist)
        self.button = QPushButton(self)
        self.button.setText("Delete author")
        self.button.clicked.connect(self.on_delete)
        self.box.layout.addWidget(self.button)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)
        self.tlist.itemClicked.connect(self.onItemClicked)
        self.update()

    def onItemClicked(self, item):
        self.current_author = item.author_id

    def update(self):
        all_authors = self.view.app.get_all_authors(aid=True)
        for author in all_authors:
            item = QListWidgetItem(author["name"])
            item.author_id = author["id"]
            self.tlist.addItem(item)

    def clear(self):
        self.tlist.clear()

    def on_delete(self):
        if self.current_author != -1:
            todel = True
            if self.view.app.check_needed(self.current_author,"author"):
                if not gen_yes_no_dialog(self.view.central.mainwin,"Delete author","Selected author is used by at least one paper\nDelete it anyway?"):
                    todel = False
            if todel:
                self.view.app.delete_author(self.current_author)
                self.clear()
                self.update()

                

    
class PublicationList(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.view = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.current_pub = -1
        self.layout = QVBoxLayout()
        self.box = QGroupBox("Delete one publication")
        self.box.layout = QVBoxLayout()
        self.tlist = QListWidget(self)
        self.box.layout.addWidget(self.tlist)
        self.button = QPushButton(self)
        self.button.setText("Delete publication")
        self.button.clicked.connect(self.on_delete)
        self.box.layout.addWidget(self.button)
        self.box.setLayout(self.box.layout)
        self.layout.addWidget(self.box)
        self.setLayout(self.layout)
        self.tlist.itemClicked.connect(self.onItemClicked)
        self.update()

    def onItemClicked(self, item):
        self.current_pub = item.pub_id

    def update(self):
        all_pubs = self.view.app.get_all_publications(pubid=True)
        for pub in all_pubs:
            item = QListWidgetItem(pub["name"])
            item.pub_id = pub["id"]
            self.tlist.addItem(item)

    def clear(self):
        self.tlist.clear()

    def on_delete(self):
        if self.current_pub != -1:
            todel = True
            if self.view.app.check_needed(self.current_pub,"publication"):
                if not gen_yes_no_dialog(self.view.central.mainwin,"Delete publication","Selected publication is used by at least one paper\nDelete it anyway?"):
                    todel = False
            if todel:
                self.view.app.delete_publication(self.current_pub)
                self.clear()
                self.update()


    
