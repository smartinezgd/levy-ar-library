#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QAbstractItemView, QLineEdit, QPushButton, QListWidget, QListWidgetItem, QTextEdit, QFileDialog
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,pyqtSignal
from levyar.ui.widgets import gen_yes_no_dialog,gen_info_dialog
from levyar.ui.icons import get_icons_path
import os

icons = get_icons_path()

class AddPaperWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.title = "Add new paper"
        self.left = 100
        self.top = 80
        self.width = 500
        self.height = 750
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = AddPaperCentral(self)
        self.setCentralWidget(self.central)

    def close_w(self):
        self.setWindowTitle(self.title)
        self.close_sig.emit()
        self.close()

    def edit(self,paper):
        self.setWindowTitle("Edit paper")
        self.central.paper = paper
        self.central.reset()
        self.central.reset_new_paper()
        self.central.update()


    def new_paper(self):
        self.setWindowTitle(self.title)
        self.central.paper = None
        self.central.reset()
        self.central.reset_new_paper()
        
        
class AddPaperCentral(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.win = parent
        self.paper = None
        self.new_paper = None
        self.reset_new_paper()
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        #Title
        self.titleinp = TitleInput(self)
        self.layout.addWidget(self.titleinp)
        #Path
        self.pathinp = PathInput(self)
        self.layout.addWidget(self.pathinp)
        #Authors
        self.authinp = AuthorsInput(self)
        self.layout.addWidget(self.authinp)
        #Publisher
        self.publishinp = PublishInput(self)
        self.layout.addWidget(self.publishinp)
        #Tags
        self.taginp = TagInput(self)
        self.layout.addWidget(self.taginp)
        #Notes
        self.notesinp = NotesInput(self)
        self.layout.addWidget(self.notesinp)
        #Notes
        self.bibtexinp = BibInput(self)
        self.layout.addWidget(self.bibtexinp)
        #Buttons
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.validatebtn = QPushButton(self.buttons)
        self.validatebtn.setText("Validate")
        self.validatebtn.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.validatebtn)
        self.cancelbtn = QPushButton(self.buttons)
        self.cancelbtn.setText("Cancel")
        self.cancelbtn.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.cancelbtn)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)
        self.layout.addStretch(1)
        self.setLayout(self.layout)

    def reset_new_paper(self):
        if self.paper:
            self.new_paper = self.copy_paper(self.paper)
        else:
            self.new_paper = {"title":"","authors":[],"publish":"","tags":[],"notes":"","id":-1,"path":"","bibtex":""}

    def copy_paper(self,paper):
        return {"title":paper["title"],
                "authors":[a for a in paper["authors"]],
                "publish":paper["publish"],
                "tags":[t for t in paper["tags"]],
                "notes":paper["notes"],
                "id":paper["id"],
                "path":paper["path"],
                "bibtex":paper["bibtex"]}
    
    def update(self):
        self.titleinp.update()
        self.authinp.update()
        self.publishinp.update()
        self.taginp.update()
        self.notesinp.update()
        self.pathinp.update()
        self.bibtexinp.update()

    def reset(self):
        self.titleinp.reset()
        self.authinp.reset()
        self.publishinp.reset()
        self.taginp.reset()
        self.notesinp.reset()
        self.pathinp.reset()
        self.bibtexinp.reset()
        
    def cancel(self):
        self.win.close_w()

    def validate(self):
        if not os.path.exists(self.new_paper["path"]):
                gen_info_dialog(self.win,"Error when saving paper","Paper path "+self.new_paper["path"]+" does not exist")
                return False
        if self.new_paper["title"].strip() == "":
            gen_info_dialog(self.win,"Error when saving paper","No title was given to the paper")
            return False
        if self.paper:
            upd = {"pid":self.new_paper["id"]}
            if self.new_paper["title"] != self.paper["title"]:
                upd["title"] = self.new_paper["title"]
            if self.new_paper["notes"] != self.paper["notes"]:
                upd["notes"] = self.new_paper["notes"]
            if self.new_paper["path"] != self.paper["path"]:
                new_path = self.win.app.copy_paper_file(self.new_paper["path"])
                upd["path"] = new_path
            if self.new_paper["bibtex"] != self.paper["bibtex"]:
                upd["bibtex"] = self.new_paper["bibtex"]
            if self.new_paper["publish"] != self.paper["publish"]:
                upd["publish"] = self.new_paper["publish"]
                upd["oldpublish"] = self.paper["publish"]
            inter = set(self.paper["tags"]) & set(self.new_paper["tags"])
            rmv = list(set(self.paper["tags"]) - inter)
            add = list(set(self.new_paper["tags"]) - inter)
            if len(rmv) >0:
                upd["rmtag"] = rmv
            if len(add) >0:
                upd["addtag"] = add
            inter = set(self.paper["authors"]) & set(self.new_paper["authors"])
            rmv = list(set(self.paper["authors"]) - inter)
            add = list(set(self.new_paper["authors"]) - inter)
            if len(rmv) >0:
                upd["rmauthor"] = rmv
            if len(add) >0:
                upd["addauthor"] = add
            self.win.app.update_paper(upd)
        else:
            new_path = self.win.app.copy_paper_file(self.new_paper["path"])
            self.new_paper["path"] = new_path
            self.win.app.insert_paper(self.new_paper)
        self.win.close_w()
    

class TitleInput(QWidget):
    def  __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("Title")
        self.layout.addWidget(self.head)
        self.tinput = QLineEdit(self)
        self.tinput.textChanged.connect(self.on_title_change)
        self.layout.addWidget(self.tinput)
        self.setLayout(self.layout)

    def update(self):
        if self.central.paper:
            self.tinput.setText(self.central.new_paper["title"])

    def on_title_change(self):
        self.central.new_paper["title"] = self.tinput.text().strip().replace("\n","").replace("\r","")

    def reset(self):
        self.tinput.setText("")

        
class AuthorsInput(QWidget):
    def  __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.headwdg = QWidget(self)
        self.headwdg.layout = QVBoxLayout()
        self.head = QLabel(self.headwdg)
        self.head.setFont(self.bold)
        self.head.setText("Authors")
        self.headwdg.layout.addWidget(self.head)
        self.addAuthor = QPushButton(self.headwdg)
        self.addAuthor.setText("New Author")
        self.addAuthor.clicked.connect(self.new_author)
        self.headwdg.layout.addWidget(self.addAuthor)
        self.headwdg.layout.addStretch(1)
        self.headwdg.setLayout(self.headwdg.layout)
        self.layout.addWidget(self.headwdg)
        self.authors = QListWidget(self)
        self.authors.setFixedHeight(100)
        self.authors.setSelectionMode(QAbstractItemView.MultiSelection)
        self.authors.itemClicked.connect(self.onAuthorClicked)
        self.layout.addWidget(self.authors)
        self.setLayout(self.layout)
        self.addAuthorWin = AddAuthorWindow(self)
        self.addAuthorWin.close_sig.connect(self.onAddAuthorClose)
        self.all_authors = []

    def new_author(self):
        self.addAuthorWin.reset(self.all_authors)
        self.addAuthorWin.show()

    def onAuthorClicked(self,item):
        auth = item.text()
        if auth not in self.central.new_paper["authors"]:
            self.central.new_paper["authors"].append(auth)
        else:
            self.central.new_paper["authors"].remove(auth)


    def update(self):
        n = self.authors.count()
        for i in range(n):
            a = self.authors.item(i).text()
            for auth in self.central.new_paper["authors"]:
                if auth == a:
                    self.authors.item(i).setSelected(True)
        
    def reset(self):
        self.all_authors = self.central.win.app.get_all_authors()
        self.authors.clear()
        for a in self.all_authors:
            self.authors.addItem(QListWidgetItem(a))

    def onAddAuthorClose(self):
        self.reset()
        self.update()


class PublishInput(QWidget):
    def  __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.headwdg = QWidget(self)
        self.headwdg.layout = QVBoxLayout()
        self.head = QLabel(self.headwdg)
        self.head.setFont(self.bold)
        self.head.setText("Published in")
        self.headwdg.layout.addWidget(self.head)
        self.addPub = QPushButton(self.headwdg)
        self.addPub.setText("New Publication")
        self.addPub.clicked.connect(self.new_pub)
        self.headwdg.layout.addWidget(self.addPub)
        self.headwdg.layout.addStretch(1)
        self.headwdg.setLayout(self.headwdg.layout)
        self.layout.addWidget(self.headwdg)
        self.publications = QListWidget(self)
        self.publications.setFixedHeight(100)
        self.publications.itemClicked.connect(self.onPubClicked)
        self.layout.addWidget(self.publications)
        self.setLayout(self.layout)
        self.addPubWin = AddPubWindow(self)
        self.addPubWin.close_sig.connect(self.onAddPubClose)
        self.all_publications = []

    def new_pub(self):
        self.addPubWin.reset(self.all_publications)
        self.addPubWin.show()

    def onPubClicked(self,item):
        pub = item.text()
        self.central.new_paper["publish"] = pub

    def update(self):
        n = self.publications.count()
        for i in range(n):
            a = self.publications.item(i).text()
            if self.central.new_paper["publish"] == a:
                self.publications.item(i).setSelected(True)
                break
        
    def reset(self):
        self.all_publications = self.central.win.app.get_all_publications()
        self.publications.clear()
        for a in self.all_publications:
            self.publications.addItem(QListWidgetItem(a))

    def onAddPubClose(self):
        self.reset()
        self.update()

     
        
class TagInput(QWidget):
    def  __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.headwdg = QWidget(self)
        self.headwdg.layout = QVBoxLayout()
        self.head = QLabel(self.headwdg)
        self.head.setFont(self.bold)
        self.head.setText("Tags")
        self.headwdg.layout.addWidget(self.head)
        self.addTag = QPushButton(self.headwdg)
        self.addTag.setText("New Tag")
        self.addTag.clicked.connect(self.new_tag)
        self.headwdg.layout.addWidget(self.addTag)
        self.headwdg.layout.addStretch(1)
        self.headwdg.setLayout(self.headwdg.layout)
        self.layout.addWidget(self.headwdg)
        self.tags = QListWidget(self)
        self.tags.setFixedHeight(100)
        self.tags.setSelectionMode(QAbstractItemView.MultiSelection)
        self.tags.itemClicked.connect(self.onTagClicked)
        self.layout.addWidget(self.tags)
        self.setLayout(self.layout)
        self.addTagWin = AddTagWindow(self)
        self.addTagWin.close_sig.connect(self.onAddTagClose)
        self.all_tags = []

    def new_tag(self):
        self.addTagWin.reset(self.all_tags)
        self.addTagWin.show()

    def onTagClicked(self,item):
        tag = item.text()
        if tag not in self.central.new_paper["tags"]:
            self.central.new_paper["tags"].append(tag)
        else:
            self.central.new_paper["tags"].remove(tag)
 

    def update(self):
        n = self.tags.count()
        for i in range(n):
            t = self.tags.item(i).text()
            for tag in self.central.new_paper["tags"]:
                if tag == t:
                    self.tags.item(i).setSelected(True)
        
    def reset(self):
        self.all_tags = self.central.win.app.get_all_tags()
        self.tags.clear()
        for t in self.all_tags:
            self.tags.addItem(QListWidgetItem(t))

    def onAddTagClose(self):
        self.reset()
        self.update()

    
class NotesInput(QWidget):
    def  __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("Notes")
        self.head.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.head)
        self.pinput = QTextEdit(self)
        self.pinput.setFixedHeight(80)
        self.pinput.textChanged.connect(self.on_notes_change)
        self.layout.addWidget(self.pinput)
        self.setLayout(self.layout)

    def update(self):
        if self.central.paper:
            self.pinput.setPlainText(self.central.new_paper["notes"])

    def on_notes_change(self):
        self.central.new_paper["notes"] = self.pinput.toPlainText().strip()

    def reset(self):
        self.pinput.setPlainText("")


class BibInput(QWidget):
    def  __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("BibTeX")
        self.head.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.head)
        self.pinput = QTextEdit(self)
        self.pinput.setFixedHeight(80)
        self.pinput.textChanged.connect(self.on_bib_change)
        self.layout.addWidget(self.pinput)
        self.setLayout(self.layout)

    def update(self):
        if self.central.paper:
            self.pinput.setPlainText(self.central.new_paper["bibtex"])

    def on_bib_change(self):
        self.central.new_paper["bibtex"] = self.pinput.toPlainText().strip()

    def reset(self):
        self.pinput.setPlainText("")


        
        
class PathInput(QWidget):
    def  __init__(self,parent):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("File Path")
        self.layout.addWidget(self.head)
        self.pathinput = QPushButton(self)
        self.pathinput.setText("Select Path")
        self.pathinput.clicked.connect(self.select_path)
        self.pathinput.setFixedHeight(30)
        self.pathinput.setFixedWidth(360)
        self.layout.addWidget(self.pathinput)
        self.setLayout(self.layout)

    def update(self):
        if self.central.paper:
            s = ""
            if len(self.central.new_paper["path"]) > 60:
                s = self.central.new_paper["path"][0:57]+"..."
            else:
                s = self.central.new_paper["path"]
            self.pathinput.setText(s)

    def select_path(self):
        path = QFileDialog.getOpenFileName(self, 'Select paper file', '.',filter=('PDF (*.pdf);;Doc (*.doc *.docx)'))[0]
        if path and os.path.exists(path):
            s = ""
            if len(path) > 60:
                s = path[0:57]+"..."
            else:
                s = path
            self.pathinput.setText(s)
            self.central.new_paper["path"] = os.path.abspath(path)
            # if gen_yes_no_dialog(self.central.win,"Import meta-data?","Do you want to import meta-data from the PDF file?"):
            #     self.central.win.app.import_pdf_metadata(self.central.new_paper)
            #     self.central.update()
        
    def reset(self):
        self.pathinput.setText("Select Path")
  
 
class AddTagWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.title = "Add new tag"
        self.left = 120
        self.top = 100
        self.width = 300
        self.height = 60
        self.tagWidget = parent
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = AddTagCentral(self)
        self.setCentralWidget(self.central)
        self.elem = ""
        self.all_tags = []

    def close_w(self):
        self.close_sig.emit()
        self.close()

    def reset(self,tags):
        self.all_tags = tags
        self.elem = ""
        self.central.reset()

    def validate(self):
        e = self.elem.strip().replace("\n","").replace("\r","")
        if e != "" and e not in self.all_tags:
            self.tagWidget.central.win.app.insert_tag(e)
            self.tagWidget.central.new_paper["tags"].append(e)
        self.close_w()

class AddTagCentral(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.win = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("Type new tag name")
        self.layout.addWidget(self.head)
        self.tinput = QLineEdit(self)
        self.tinput.textChanged.connect(self.on_text_change)
        self.layout.addWidget(self.tinput)
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.validatebtn = QPushButton(self.buttons)
        self.validatebtn.setText("Add")
        self.validatebtn.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.validatebtn)
        self.cancelbtn = QPushButton(self.buttons)
        self.cancelbtn.setText("Cancel")
        self.cancelbtn.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.cancelbtn)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)
        self.layout.addStretch(1)
        self.setLayout(self.layout)


    def on_text_change(self):
        self.win.elem = self.tinput.text().strip()
        
    def reset(self):
        self.tinput.setText("")

    def cancel(self):
        self.win.close_w()

    def validate(self):
        self.win.validate()
        

class AddAuthorWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.title = "Add new author"
        self.left = 120
        self.top = 100
        self.width = 300
        self.height = 60
        self.authWidget = parent
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = AddTagCentral(self)
        self.setCentralWidget(self.central)
        self.central.head.setText("Type new author name")
        self.elem = ""
        self.all_authors = []

    def close_w(self):
        self.close_sig.emit()
        self.close()

    def reset(self,authors):
        self.all_authors = authors
        self.elem = ""
        self.central.reset()

    def validate(self):
        e = self.elem.strip().replace("\n","").replace("\r","")
        if e != "" and e not in self.all_authors:
            self.authWidget.central.win.app.insert_author(e)
            self.authWidget.central.new_paper["authors"].append(e)
        self.close_w()


class AddPubWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.title = "Add new publication"
        self.left = 120
        self.top = 100
        self.width = 300
        self.height = 60
        self.pubWidget = parent
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = AddPubCentral(self)
        self.setCentralWidget(self.central)
       
        self.elem = ""
        self.all_publications = []

    def close_w(self):
        self.close_sig.emit()
        self.close()

    def reset(self,publications):
        self.all_publications = publications
        self.elem = ""
        self.central.reset()

    def validate(self):
        e = self.elem.strip().replace("\n","").replace("\r","")
        if e != "" and e not in self.all_publications:
            self.pubWidget.central.win.app.insert_publication(e)
            self.pubWidget.central.new_paper["publish"] = e
            
        self.close_w()
        

class AddPubCentral(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.win = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.head = QLabel(self)
        self.head.setFont(self.bold)
        self.head.setText("Type new publication")
        self.layout.addWidget(self.head)
        self.pinput = QTextEdit(self)
        self.pinput.setFixedHeight(50)
        self.pinput.textChanged.connect(self.on_text_change)
        self.layout.addWidget(self.pinput)
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.validatebtn = QPushButton(self.buttons)
        self.validatebtn.setText("Add")
        self.validatebtn.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.validatebtn)
        self.cancelbtn = QPushButton(self.buttons)
        self.cancelbtn.setText("Cancel")
        self.cancelbtn.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.cancelbtn)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)
        self.layout.addStretch(1)
        self.setLayout(self.layout)


    def on_text_change(self):
        self.win.elem = self.pinput.toPlainText().strip()

    def reset(self):
        self.pinput.setPlainText("")
        
    def cancel(self):
        self.win.close_w()

    def validate(self):
        self.win.validate()
