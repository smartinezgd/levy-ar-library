#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel, QTabWidget, QLineEdit, QPushButton, QFileDialog, QSizePolicy
from PyQt5.QtGui import QPixmap,QFont,QCursor,QBrush,QPixmap,QIcon
from PyQt5.QtCore import Qt,QSize
from levyar.ui.icons import get_icons_path
from levyar.ui.papers import PapersView
from levyar.ui.search import SearchView
from levyar.ui.delete import DeleteView
import os

icons = get_icons_path()


class CentralView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.mainwin = parent
        self.layout = QVBoxLayout()
        self.tabs = QTabWidget()
        self.papersview = PapersView(self)
        self.searchview = SearchView(self)
        self.deleteview = DeleteView(self)
        self.tabs.addTab(self.papersview,"Papers")
        self.tabs.addTab(self.searchview,"Search")
        self.tabs.addTab(self.deleteview,"Delete")
        self.tabs.currentChanged.connect(self.onChange)
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)
        self.searched = False

    def clear(self):
        self.papersview.clear()

    def update(self):
        self.papersview.update()
  
    def gotoTab(self,i):
        self.tabs.setCurrentIndex(i)

    def onChange(self,i):
        if i == 0:
            self.papersview.clear()
            self.papersview.update()
        elif i == 1:
            self.searchview.clear()
            self.searchview.update()
        elif i == 2:
            self.deleteview.clear()
            self.deleteview.update()

    def global_refresh(self):
        self.searched = False
        self.deleteview.clean()
        self.searchview.reset_query()
        self.papersview.clean()
        self.app.set_view({})

    def setTop(self,msg):
        self.searched = True
        self.papersview.setTop(msg)
        self.deleteview.setTop(msg)
