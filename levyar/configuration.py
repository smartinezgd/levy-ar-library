#    This file is part of Levy AR Library
#    Copyright (C) 2020 Sébastien Martinez
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
        

import yaml
import os

class Configuration(object):
    def __init__(self):
        self.file_path = os.path.join(os.environ["HOME"],".levyar/library.cfg")
        self.papers_root = os.path.join(os.environ["HOME"],".levyar/")
        self.db_path = os.path.join(os.environ["HOME"],".levyar/library.db")
        self.pdf_command = "xdg-open %f"
        self.virtual = True #True if the configuration is not loaded
        
    def save_config(self):
        d = {"papers":self.papers_root,"database":self.db_path,"command":self.pdf_command}
        f = open(self.file_path,"w")
        yaml.dump(d,f,default_flow_style=False)
        f.close()
        self.virtual = False

    def load_config(self,cfg_path):
        if not os.path.exists(cfg_path):
            return cfg_path+" does not exist"
        else:
            with open(cfg_path,"r") as cfgfile:
                conf = yaml.safe_load(cfgfile)
                if "papers" not in conf.keys() or "database" not in conf.keys() or "command" not in conf.keys():
                    return cfg_path+" is not a compatible configuration file"
                else:
                    self.papers_root = conf["papers"]
                    self.db_path = conf["database"]
                    self.file_path = cfg_path
                    self.pdf_command = conf["command"]
            self.virtual = False
            return self.validate()

    def clone(self):
        c = Configuration()
        c.file_path = self.file_path
        c.papers_root = self.papers_root
        c.db_path = self.db_path
        c.pdf_command = self.pdf_command
        c.virtual = self.virtual
        return c

    def validate(self):
        if not os.path.exists(self.papers_root):
            return "Paper root "+str(self.papers_root)+" does not exist"
        return "ok"
