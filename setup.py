#!/usr/bin/env python3

from distutils.core import setup
import os

icons=["levyar/ui/icons/*"]

setup(name="Levy AR Library",
      version="1.0",
      description="Scientific Publication Library",
      scripts=["bin/levylibrary"],
      packages=["levyar","levyar.ui"],
      package_dir={"levyar.ui": "levyar/ui"},
      package_data = {'levyar.ui' : ["icons/*.png"] },
      license="special",
)
      
